from sklearn.externals import joblib
from abc import ABC, abstractmethod


class IntentClassifier(ABC):

    @abstractmethod
    def predict_one(self):
        pass

    @abstractmethod
    def predict(self):
        pass


class StatisticalIntentClassifier(IntentClassifier):

    def __init__(self, language):
        if language == "en":
            self.clf = joblib.load('../models/question_classifier_en.pkl')
        else:
            raise ValueError("No model for language", language)

    def predict_one(self, question):
        return self.clf.predict([question.raw])

    def predict(self, list_of_questions):
        return self.clf.predict([q.raw for q in list_of_questions])


class RuleBasedIntentClassifier(IntentClassifier):

    def __init__(self, language):
        if language != "en":
            raise ValueError("No model for language", language)

    def predict_one(self, text):
        if text.raw.lower().startswith("who"):
            return "HUM:desc"
        elif text.raw.lower().startswith("how many"):
            return "NUM:count"
        else:
            return "NONE"

    def predict(self, texts):
        return [self.predict_one(text) for text in texts]



class ConditionIdentifier:

    def __init__(self, language):
        if self.language == "en":
            self.intent_classifier = StatisticalIntentClassifier("en")

    def get_condition(self, text):

        target = self.intent_classifier.predict_one(text.raw)
        operator = extract_operator(target)
        value = extract_value(target, operator)

        return Condition(target=target, operator=operator, value=value)
    
