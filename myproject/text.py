from myproject import preprocess


class Text:

    def __init__(self, raw_string, language):
        self.raw = raw_string
        self.language = language
        self.pos = None
        self.tokens = None
        self.lemmas = None

    def tokenize(self):
        self.tokens = preprocess.tokenize(self.raw, self.language)

    def lemmatize(self):
        self.lemmas = preprocess.lemmatize(self.tokens, self.language)

    def pos_tag(self):
        self.pos = preprocess.pos_tag(self.tokens, self.language)

    def preprocess(self):
        self.tokenize()
        self.lemmatize()
        self.pos_tag()

    def preprocess_in_one_step(self):
        self.tokens, self.pos, self.lemmas = preprocess.preprocess(self.raw)


