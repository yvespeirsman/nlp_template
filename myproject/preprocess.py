import spacy
import nltk


# It is possible to define methods that do one preprocessing task.

def tokenize(text, language):
    return nltk.word_tokenize(text, language)


def pos_tag(tokens, language):
    pass


def lemmatize(tokens, language):
    pass


# Sometimes it is better to define a method that does all preprocessing tasks
# in one go, because this is what the underlying library does.
# One good example is the spaCy library. You can choose to work with spaCy's
# text object in your pipeline, or you can take the relevant spaCy attributes
# and put them on your pipeline. This can be better if different languages require
# you to work with different preprocessors.

en = spacy.load("en")


def preprocess(text):
    doc = en(text)
    tokens = [t.orth_ for t in doc]
    pos = [t.pos_ for t in doc]
    lemmas = [t.lemma_ for t in doc]

    return (tokens, pos, lemmas)
