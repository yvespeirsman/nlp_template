from myproject.text import Text
from myproject.frame import FrameConstructor

user_question = "How many visitors did I have yesterday?"

# Preprocess the text

text = Text(user_question, "en")
text.preprocess_in_one_step()
print(text.raw)
print(text.tokens)
print(text.pos)
print(text.lemmas)

# Define the frame construction pipeline

constructor_en = FrameConstructor("en")
constructor_nl = FrameConstructor("nl")


frame = constructor_en.construct_frame(text)


